package gui;

import java.awt.Color;
import java.awt.Font;

import logic.Decoder;
import logic.Encoder;

/**
 * GUI is the driver class which consists of the main method which initializes the {@link gui.SplashScreen} and
 * the JFrame {@link gui.ContentWrapper}.
 * 
 * 
 * @author      Subham Kharel
 * @version     1.1
 * @since       1.0
 */

public class GUI
{
	
	protected static SplashScreen splash;

	protected static Font[] fonts = {
        new Font("IMPACT", Font.BOLD, 40),
        new Font("Consolas", Font.PLAIN, 25),
        new Font("Calibri", Font.BOLD, 35)
    }; // Fonts to be used for this application

	protected static Color black = Color.decode("#000000");
	protected static Color white = Color.decode("#ffffff");
    
    /**
     * Initializes the frame and the loading screen for the application.
     * Basically turning into a vicious cycle of method calls and constructor cry outs. 
     * 
     * @param args the command line arguments
     * @return No return value
     */
    
    public static void main(String[] args)
    {
        ContentWrapper content = new ContentWrapper(new Encoder(), new Decoder(), white, black);
        
    	splash = new SplashScreen(content, white, black, black, 2);
        
        content.initialize(); // Initializes the application's frame
        
    }
    
}
