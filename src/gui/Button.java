package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JButton;

/**
 * {@link gui.Button} extends JButton which creates a new JButton.
 * 
 * @author      Subham Kharel
 * @version     1.1
 * @since       1.0
 * @see         {@link javax.swing.JButton}
 */

public class Button extends JButton
{	
	private static final long serialVersionUID = 1L;
	
	/**
     * A constructor for {@link gui.Button}.
     * 
     * @param title		title of the JButton
     * @param bg      	background color
     * @param fg  		foreground color
     * 
     * 
     * @author      	Subham Kharel
     * @version    		1.1
     * @since       	1.0
     * @return			No return value.
     * @see         	{@link javax.swing.UIManager}
     */
	
	public Button(String title, Color bg, Color fg)
	{
		super(title);
		
		setIcon(new PngIcon(title));
		
		setBackground(bg);
		
	    setForeground(fg);
	    
	    setFocusPainted(false);
	    
	    setFont(new Font("Consolas", Font.PLAIN, 15));
	    
	    setPreferredSize(new Dimension(180, 50));
	    
	    setFocusPainted(false);
	    
	    setBorderPainted(false);
	    
	}
}
