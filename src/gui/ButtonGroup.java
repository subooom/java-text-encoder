package gui;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JPanel;

import logic.Decoder;
import logic.Encoder;
import logic.FileImporter;
import logic.listener.button.CreateListener;
import logic.listener.button.DecodeListener;
import logic.listener.button.EncodeListener;
import logic.listener.button.GenerateHashListener;
import logic.listener.button.ShowStatsListener;

/**
 * {@link gui.ButtonGroup} is a panel which consists of four buttons as of right now.
 * This can be extended upto many buttons but you might have to
 * fiddle with the <code>makeLayout()</code> method of {@link gui.DataPanel}.
 * This panel consists of:
 * <ul>
 * 		<li>Four {@link gui.Button} instances</li>
 * </ul>
 * 
 * @author      Subham Kharel
 * @version     1.1
 * @since       1.0
 * @see         {@link javax.swing.JButton}, {@link javax.swing.JPanel}
 */

public class ButtonGroup extends JPanel
{	
	private static final long serialVersionUID = 1L;
 
    protected Encoder encoder;
	protected Decoder decoder;
    protected DataBoard dataBoard;
    protected DataPanel dataPanel;
    protected Statistic statObject;
    protected FileImporter fileChooser;
    protected Button create, encode, decode, generateHash, stats; // The four instances instantiated here
    
     /**
	 * A constructor for {@link gui.ButtonGroup}.
	 * It initializes all the required components and all five {@link gui.Button} instances.
	 *
	 * 
	 * @param font      font for the text in the JButtons
	 * @param dp      	an instance of {@link gui.DataPanel}
	 * @param stats		an instance of {@link gui.Statistic}
	 * 
	 * @return 			No return value.
	 * 
	 * @since       	1.0
	 * @see         	{@link javax.swing.JButton}
	 */
    
	public ButtonGroup(Font font, DataPanel dp, Statistic stats)
	{
		this.dataBoard = dp.getDataBoard();
		this.dataPanel = dp;
		this.statObject = stats;
		
		this.create = new Button("create", new Color(59, 182, 152), Color.WHITE);
		this.encode = new Button("encode", Color.decode("#3B59B6"), Color.WHITE);
		this.decode = new Button("decode", new Color(89, 182, 59), Color.WHITE);
		this.generateHash = new Button("generate hash", new Color(182, 90, 59), Color.WHITE);
		this.stats = new Button("show stats", new Color(182,59,89), Color.WHITE);
		this.encoder = dp.encoder;
		this.decoder = dp.decoder;

        makeLayout();
	}
	
	/**
	* This adds all the button to {@link gui.ButtonGroup} panel
	* and also fires the member method <code>listenEvents()</code> to handle the events.
	*
	* 
	* @return 			No return value.
	* 
	* @since       	1.0
	* @see         	{@link javax.swing.JButton}, {@link java.awt.event.MouseListener}, {@link java.awt.event.MouseEvent}
	*/
	
	private void makeLayout()
	{
		listenEvents();
		add(create);
		add(encode);
        add(decode);
        add(generateHash);
        add(stats);
        setOpaque(false);
	}

	/**
	* This Listens for {@link java.awt.event.MouseEvent}s and
	* fires the respective MouseListener class.
	*
	* 
	* @return 			No return value.
	* 
	* @since       	1.0
	* @see         	{@link javax.swing.JButton}, {@link java.awt.event.MouseListener}, {@link java.awt.event.MouseEvent}
	* 
	*/
	
 	private void listenEvents()
 	{
        create.addMouseListener(new CreateListener(create, dataPanel)); 		
        encode.addMouseListener(new EncodeListener(encode, dataBoard, dataPanel, encoder, decoder));
        decode.addMouseListener(new DecodeListener(decode, dataBoard, dataPanel, decoder, encoder));
        generateHash.addMouseListener(new GenerateHashListener(generateHash, dataBoard, dataPanel, statObject));
        stats.addMouseListener(new ShowStatsListener(statObject, dataPanel, generateHash));
	}

}
