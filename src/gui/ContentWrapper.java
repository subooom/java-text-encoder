package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.*;

import logic.Decoder;
import logic.Encoder;

import gui.menu.MenuBar;

/**
 * ContentWrapper is the main frame which consists of:
 * <ul><li>{@link gui.menu.MenuBar}</li>
 * 	<li>{@link gui.DataPanel}</li>
 * </ul>
 *
 *
 * @author      Subham Kharel
 * @version     1.1
 * @since       1.0
 * @see         {@link javax.swing.JFrame}
 * @see         {@link javax.swing.JPanel}
 * @see         {@link javax.swing.JMenu}
 */

public class ContentWrapper extends JFrame
{

	private static final long serialVersionUID = 1L;

	protected Encoder encoder;
	protected Decoder decoder;

    protected Color background;
    protected Color foreground;

	protected static String headTitle = "DoughDish Text Encryption";
	protected static String title = "two-bit encyptor";

    protected DataPanel dataPanel; // The panel consisting of the textarea
    protected MenuBar menuBar;

	protected Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize(); // Get the screen size

	protected int width = 600;
    protected int height = 750;

    protected int posx = (int) (screenSize.getWidth()/2) - (int) (width/2); // X Position of the frame
    protected int posy = (int) (screenSize.getHeight()/2) - (int) (height/2); // Y Position of the frame

    /**
    * A constructor for class {@link gui.ContentWrapper}.
    * @param encoder  the object of {@link logic.Encoder} class instantiated in the driver class {@link gui.GUI}
    * @param decoder  the object of {@link logic.Decoder} class instantiated in the driver class {@link gui.GUI} as well
    * @param bg       background color for the JFrame
    * @param fg       foreground color for the JFrame
    * @return         No return value
    * @see            {@link logic.Encoder}, {@link logic.Decoder}
    * @since          1.0
    */

    public ContentWrapper(Encoder encoder, Decoder decoder, Color bg, Color fg)
    {
        super(headTitle);  // Accessing and modifying the parent constructor by using the keyword super

        this.background = bg;
        this.foreground = fg;

        this.encoder = encoder;
        this.decoder = decoder;

        this.dataPanel =  new DataPanel(bg, fg, this.encoder, this.decoder);
        this.menuBar = new MenuBar(dataPanel);

        setIconImage(Toolkit.getDefaultToolkit().getImage("res/icon.png")); // Setting the icon

    }

    /**
    * Adds the {@link gui.Datapanel} object to the main frame.
    * @return         No return value
    * @see            {@link DataPanel}
    * @since          1.0
    */

    public void initialize()
    {

        add(dataPanel); // adding the container panel into the frame

        this.makeLayout();
    }

    /**
     * Sets the default close operation, size, location and a {@link gui.menu.Menubar} for our frame.
     * It also sets the resizability of the frame to true.
     *
     * @return         No return value
     * @since          1.0
     */

    protected void makeLayout()
    {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(width, height);
        setLocation(posx, posy);
        setJMenuBar(menuBar);
        setResizable(true);
        setVisible(true);

    }
}
