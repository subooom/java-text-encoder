/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 * {@link gui.WelcomeMessage} extends JLabel.
 *
 * 
 * @author      Subham Kharel
 * @version     1.1
 * @since       1.0
 * @see         {@link javax.swing.JLabel}
 */
public class WelcomeMessage extends JLabel
{
	private static final long serialVersionUID = 1L;

	 /**
     * A constructor for {@link gui.WelcomeMessage}.
     * 
     * @param text		text content of the JLabel
     * @param bcolor    background color
     * @param fcolor    foreground color
	 * @param font 		font for the JLabel
     * 
     * 
     * @author      Subham Kharel
     * @version     1.1
     * @since       1.0
     * @return 		No return value
     * @see         {@link java.awt.Color}, {@link java.awt.Font}, {@link javax.swing.JLabel}
     */
	
	public WelcomeMessage(String text, Color bcolor, Color fcolor, Font font)
	{
        super();
        setText(text);
        visible(true);
        setFont(font);
        setBackground(bcolor);
        setHorizontalAlignment(SwingConstants.CENTER);
        setForeground(fcolor);
    }
	
	 /**
     * A setter method to set the visibility of the {@link gui.WelcomeMessage} from remote locations.
     * 
     * @param visible	boolean value for visibility
     * 
     * 
     * @author      Subham Kharel
     * @version     1.1
     * @since       1.0
     * @return 		current instance of {@link gui.WelcomeMessage}
     * @see         {@link java.awt.Color}, {@link java.awt.Font}, {@link javax.swing.JLabel}
     */
	
    public WelcomeMessage visible (boolean visible)
    {
        setVisible(visible);
        
        return this;
    }
    
}
