package gui;
	
import java.awt.*;
import javax.swing.*;
import javax.swing.plaf.metal.MetalScrollBarUI;

/**
 * {@link gui.CustomScrollBarUI} extends MetalScrollBarUI which helps in creation of custom scroll bar.
 * I use the {@link java.awt.Graphics} class to draw on the overridden methods of the parent class.
 * 
 * @author      Subham Kharel
 * @version     1.1
 * @since       1.0
 * @see         {@link javax.swing.JButton}
 */

public class CustomScrollBarUI extends MetalScrollBarUI
{

        private JButton left, right;

		/**
		 * A constructor for {@link gui.CustomScrollBarUI}.
		 * It creates two JButtons, left and right arrow of the vertical scroll bar to be used later.
		 *  
		 * 
		 * @author      	Subham Kharel
		 * 
		 * @version    		1.2
		 * 
		 * @since       	1.1
		 * 
		 * @return			No return value.
		 * 
		 * @see         	{@link javax.swing.UIManager}
		 */
        
        public CustomScrollBarUI()
        {            
        	left = new JButton("<");
        	left.setBackground(Color.decode("#1f1f1f"));
        	left.setFont(new Font("Consolas", Font.PLAIN, 12));
        	left.setForeground(Color.WHITE);
        	left.setPreferredSize(new Dimension(25, 25));
        	left.setBorder(BorderFactory.createLineBorder(new Color(47,47,47)));
        	left.setFocusPainted(false);
        	
        	right = new JButton(">");
        	right.setBorder(BorderFactory.createLineBorder(new Color(47,47,47)));
        	right.setBackground(Color.decode("#1f1f1f"));
        	right.setFont(new Font("Consolas", Font.PLAIN, 12));
        	right.setForeground(Color.WHITE);
        	right.setPreferredSize(new Dimension(25, 25));
        	right.setFocusPainted(false);
        }

        /**
         * This draws the thumb of the scroll bar.
         * 
         * 
         * @see         {@link java.awt.Graphics}
         * @return		No return value.
         * @since       1.0
         */
        
        @Override
        protected void paintThumb(Graphics g, JComponent c, Rectangle r)
        {
            g.setColor(Color.decode("#FFFFFF"));
            ((Graphics2D) g).fillRect(r.x, r.y, r.width, r.height);
            g.setColor(Color.BLACK);
            ((Graphics2D) g).drawRect(r.x, r.y, r.width, r.height);
        }

        /**
         * This draws the track area of the scroll bar.
         * 
         * 
         * @see         {@link java.awt.Graphics}
         * @return		No return value.
         * @since       1.0
         */
        
        @Override
        protected void paintTrack(Graphics g, JComponent c, Rectangle r)
        {
            g.setColor(Color.decode("#1f1f1f"));
            ((Graphics2D) g).fillRect(r.x, r.y, r.width, r.height);
            g.setColor(Color.decode("#1f1f1f"));
            ((Graphics2D) g).drawRect(r.x, r.y, r.width, r.height);
        }

        /**
         * This sets the decrease button of the {@link MetalScrollBarUI}
         * by overriding the parent's methods.
         * 
         * 
         * @see         {@link javax.swing.JButton}
         * @return		left button
         * @since       1.0
         */
        
        @Override
        protected JButton createDecreaseButton(int orientation)
        {
            return left;
        }


        /**
         * This sets the increase button of the {@link MetalScrollBarUI}
         * by overriding the parent's methods.
         * 
         * 
         * @see         {@link javax.swing.JButton}
         * @return		right button
         * @since       1.0
         */
        
        @Override
        protected JButton createIncreaseButton(int orientation)
        {
            return right;
        }
    }
