package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JWindow;

/**
 * {@link gui.SplashScreen} extends {@link javax.swing.JWindow} which helps in creation of the loading splash screen.
 * 
 * @author      Subham Kharel
 * @version     1.2
 * @since       1.1
 * @see         {@link javax.swing.JButton}
 */

public class SplashScreen extends JWindow
{
	
	private static final long serialVersionUID = 1L;

	protected int timeToSleep;
	
	protected JPanel panel;
	protected JLabel title;
	protected JLabel bodyIcon;
	protected JLabel bodyText;
	protected JLabel footer;
	
	/**
	 * A constructor for {@link gui.SplashScreen}.
	 * It creates two JButtons, left and right arrow of the vertical scroll bar to be used later.
	 *  
	 *  @param			content		An instance of the main frame viz. {@link gui.ContentWrapper}
	 *  @param			bg 			Background color
	 *  @param			fg 			Foreground color
	 *  @param			border 		Border color 
	 *  @param			seconds 	Time before decay
	 * 
	 * @since       	1.1
	 * 
	 * @return			No return value.
	 * 
	 * @see         	{@link javax.swing.UIManager}, {@link java.awt.Color}
	 */
	
	public SplashScreen(ContentWrapper content, Color bg, Color fg, Color border, int seconds)
	{
		
		super(content); // setting up the parent container
		
		timeToSleep = seconds * 1000;
		
		panel = new JPanel();
		
		title = new Title("<html>DOUGHFISH TEXT ENCODER</html>",  new Font("Impact", Font.BOLD, 30), fg);
		
        Icon icon = new ImageIcon("res/loading.gif");
		bodyIcon = new JLabel(icon);
		
		bodyText = new Title("Loading..", new Font("Consolas", Font.BOLD, 25), new Color(182, 90, 59));
		
		footer = new Title("<html>Developed by Subham Kharel.</html>",  new Font("Calibri", Font.BOLD, 15), fg);
		
		setLayout(new FlowLayout());
		
		
		setBounds(500, 150, 400, 400);
		
		panel.setBackground(bg);
		panel.setPreferredSize(new Dimension(400, 390));
		
		addToPanel(title);
		addToPanel(bodyIcon);
		addToPanel(bodyText);
		addToPanel(footer);
		
		this.getContentPane().add(panel);
		
    	setVisible(true);
    	try {
    	    Thread.sleep(timeToSleep);
    	} catch (InterruptedException e) {
    		// do nothing
    	}
    	setVisible(false);
	}
	
	/**
     * This adds a {@link java.awt.Component} to this panel.
     * 
     * 
     * @return		No return value.
     * @since       1.1
     */
	
	protected void addToPanel(Component c)
	{
		this.panel.add(c);
	}
}
