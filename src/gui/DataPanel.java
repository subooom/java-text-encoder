/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.border.Border;

import logic.Encoder;
import logic.Decoder;

/**
 * {@link gui.DataPanel} is the panel which consists of:
 * <ul>
 * 		<li>A JLabel {@link gui.Title}</li>
 * 		<li>A JLabel {@link gui.Statistic}</li>
 * 		<li>A JPanel {@link gui.ButtonGroup}</li>
 * 		<li>A JScrollPanel {@link gui.DataBoard}</li>
 * 		<li>Another JLabel {@link gui.WelcomeMessage}</li>
 * </ul>
 * 
 * @author      Subham Kharel
 * @version     1.1
 * @since       1.0
 * @see         {@link javax.swing.JLabel}, {@link javax.swing.JPanel}, {@link javax.swing.JScrollPane}
 */

public class DataPanel extends JPanel
{

	private static final long serialVersionUID = 1L;

	protected Font[] fonts = {
        new Font("IMPACT", Font.BOLD, 35),
        new Font("Consolas", Font.PLAIN, 25),
        new Font("Calibri", Font.PLAIN, 20)
    };
    protected Color colorB, colorW; 

	protected Encoder encoder;
	protected Decoder decoder;
	
    protected ButtonGroup buttonGroup;
    protected WelcomeMessage welcomeMsg;
    
    protected DataBoard dataBoard;
    
    protected Title titleObject;
    
    protected Statistic stats;
    
    protected String outputMethod; // For statistics    
    protected int inputLength, outputLength; // For statistics    
    
    // Welcome message
    protected String welMsg = "<html>Goto <code>File > Load</code> to import a text file.<br />"
    		+ "Or <code>create</code> a new one.</html>"; 
    
    /**
     * A constructor for {@link gui.DataPanel}. It initializes all the required component in {@link gui.DataPanel}.
     * We use {@link javax.swing.UIManager} to set a custom scrollbar {@link gui.CustomScrollBarUI}
     * to the ScrollPane {@link DataBoard}.
     * 
     * @param cb      blue color passed in from {@link gui.ContentWrapper}
     * @param cw      white color passed in from {@link gui.ContentWrapper}
     * @param encoder  the instance of {@link logic.Encoder} class instantiated in {@link GUI}
     * @param decoder  the instance of {@link logic.Decoder} class instantiated in {@link GUI} as well
     * 
     * 
     * @author      Subham Kharel
     * @version     1.1
     * @since       1.0
     * @see         {@link javax.swing.UIManager}
     */
    
    public DataPanel (Color cb, Color cw, Encoder encoder, Decoder decoder)
    {
        super();
        
    	UIManager.put("CustomScrollBarUI", "my.package.CustomScrollBarUI");
    	
        colorB = cb;        
        colorW= cw;
        
        this.encoder = encoder;
        this.decoder = decoder;
        
        JTextArea textarea = new JTextArea();
        textarea.setLineWrap(true);

        Border border = BorderFactory.createLineBorder(colorW);
        textarea.setBorder(BorderFactory.createCompoundBorder(border, BorderFactory.createEmptyBorder(20, 20, 20, 20)));
        
        dataBoard = new DataBoard(textarea, colorW, colorB, fonts[2]);

        stats = new Statistic();
        
        buttonGroup = new ButtonGroup(fonts[1], this, stats);
        
        welcomeMsg = new WelcomeMessage(welMsg, colorB, colorW, fonts[1]);
        titleObject = new Title(ContentWrapper.title, fonts[0], colorW);
        

    	makeLayout();
        
        setBackground(colorB);
    }
    
    /**
     * This sets the layout of this JPanel to {@link java.awt.GridBagLayout} and arranges the components in a systematic order using {@link java.awt.GridBagConstraints}
     * 
     * 
     * @see         {@link java.awt.Color}
     * @return		No return value.
     * @since       1.0
     */
    
    public void makeLayout()
    {
        setLayout(new GridBagLayout());

        GridBagConstraints titlePosition = new GridBagConstraints();
        titlePosition.gridx = 0;
        titlePosition.gridy = 0;
        titlePosition.weightx = 1.0;
        titlePosition.weighty = 2.0;
        titlePosition.ipady = 150;
        titlePosition.ipadx = 600;
        add(titleObject, titlePosition);
        add(stats, titlePosition);
        
        GridBagConstraints buttonGroupPosition = new GridBagConstraints();
        buttonGroupPosition.gridx = 0;
        buttonGroupPosition.gridy = 4;
        buttonGroupPosition.weightx = 1.0;
        buttonGroupPosition.weighty = 2.0;
        buttonGroupPosition.ipady = 100;
        buttonGroupPosition.ipadx = 500;
        add(buttonGroup, buttonGroupPosition);
        
        GridBagConstraints welcomeMsgPosition = new GridBagConstraints();
        welcomeMsgPosition.gridx = 0;
        welcomeMsgPosition.gridy = 6;
        welcomeMsgPosition.ipady = 350;
        welcomeMsgPosition.ipadx = 600;
        add(welcomeMsg, welcomeMsgPosition);
        add(dataBoard, welcomeMsgPosition);
    }
    
    /**
     * This stores the contents of the imported text file
     * (which is imported from {@link logic.FileImporter})
     * into the JTextArea in {@link gui.DataBoard}.
     * 
     * 
     * @see         {@link javax.swing.JFileChooser}
     * @return		No return value.
     * @since       1.0
     */
    
    public void storeData (String text)
    {
        welcomeMsg.visible(false);
        titleObject.setVisible(true);
        dataBoard.text(text);
        dataBoard.visible(true);
    }
    
    /**
     * This creates an JTextArea with the text "// start typing here..."
     * after the <code>create</code> button or the <code>New</code> JMenuItem in {@link gui.menu.FileMenu} is clicked.
     * 
     * 
     * @see         {@link javax.swing.JMenu}, {@link javax.swing.JMenuItem},
     * 				{@link java.awt.event.MouseListener}, {@link java.awt.event.MouseEvent},
     * 				{@link java.awt.event.ActionListener}, {@link java.awt.event.ActionEvent}
     * @return		No return value.
     * @since       1.0
     */

    public void createNewFile()
    {
        welcomeMsg.visible(false);
        dataBoard.text("// start typing here..");
        dataBoard.visible(true);
    }
    
    /**
     * This returns the current instance of the JScrollPane {@link gui.DataBoard} for ease of access.
     * 
     * @return 		Current instance of {@link gui.DataBoard}.
     * @see         {@link javax.swing.JScrollPane} 
     */
    
    public DataBoard getDataBoard()
    {
    	return this.dataBoard;
    }
    
    /**
     * Setter method for the title text for ease of access.
     * 
     * @param 		title
     * 
     * @return 		No return Value.
     * @see         {@link javax.swing.JLabel}, {@link gui.Title}
     */
    
    public void setTitleText(String title)
    {
        titleObject.setTitleText(title);
    }
    
    // FOR STATS
    public void setInputLength(int length)
	{
		this.inputLength = length;		
	}

    // FOR STATS
    public void setOutputLength(int length)
	{
		this.outputLength = length;		
	}

    // FOR STATS
	public int getInputLength()
	{	
		return this.inputLength;
	}

    // FOR STATS
	public int getOutputLength()
	{
		return this.outputLength;
	}
	
	public void setTitleVisibility(boolean v)
	{
		titleObject.setVisible(v);
	}

    // FOR STATS
	public void setOutputMethod(String string)
	{
		this.outputMethod = string;
		
	}
	
    // FOR STATS
	public String getOutputMethod()
	{
		return this.outputMethod;
		
	}

    
}
