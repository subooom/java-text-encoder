/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import javax.swing.ImageIcon;

/** * 
 * {@link gui.PngIcon} extends the {@link javax.swing.ImageIcon} making use
 * of inheritance and basically created an ImageIcon based on the path as long as
 * the image is inside of "/res" directory and has a ".png" extension.
 * 
 * @author      Subham Kharel
 * @version     1.1
 * @since       1.0
 * @see         {@link javax.swing.JOptionPane}
 */

public class PngIcon extends ImageIcon
{
	private static final long serialVersionUID = 1L;
	
	/**
     * A constructor for {@link gui.PngIcon} which creates a new instance of JOptionPane.
     * It takes in a path and returns and ImageIcon of 20 x 20.
     * 
     * @param path			a String specifying a filename or path
     * 
     * @since       1.0
     * @return		No return value.
     * @see         {@link javax.swing.ImageIcon}
     */
	
	public PngIcon(String path)
	{
        super(new ImageIcon("res/" + path + ".png").getImage().getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH));
    }
}
