/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;


import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 * {@link gui.Title} is a simply a JLabel.
 * 
 * 
 * @author      Subham Kharel
 * @version     1.1
 * @since       1.0
 * @see         {@link javax.swing.JLabel}
 */
public class Title extends JLabel
{
	
	private static final long serialVersionUID = 1L;
	
	/**
    * A constructor for class {@link gui.Title} which sets the title, font and the foreground color of the JLabel
    * 
    * @param title  	title of JLabel
    * @param font 		font of the JLabel
    * @param color		foreground color of the text contents
    * 
    * @return         No return value
    * @since          1.0
    * @see            {@link javax.swing.JLabel}
    */

	public Title(String title, Font font, Color color)
	{
        super();
        setText(title);
        setVisible(true);
        setFont(font);
        setHorizontalAlignment(SwingConstants.CENTER);
        setForeground(color);
        setLayout(new FlowLayout());
    }
	
	/**
    * Setter method for the JLabel.
    * 
    * @param str  	title of JLabel
	*
    * @return       No return value
    * @since        1.0
    * @see          {@link javax.swing.JLabel}
    */
	
	public void setTitleText(String str)
	{		
		setText(str);		
	}
}
