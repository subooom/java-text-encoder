/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import javax.swing.JOptionPane;

/**
 * 
 * {@link gui.Modal} extends the {@link javax.swing.JOptionPane} making use
 * of inheritance.
 * 
 * @author      Subham Kharel
 * @version     1.1
 * @since       1.0
 * @see         {@link javax.swing.JOptionPane}
 */

public class Modal extends JOptionPane
{
	private static final long serialVersionUID = 1L;

	/**
     * A constructor for {@link gui.Modal} which creates a new instance of JOptionPane.
     * 
     * @param title			title of the JOptionpane
     * @param message       message of the JOptionpane
     * @param type     		the type of message to be displayed: ERROR_MESSAGE, INFORMATION_MESSAGE, etc.
     * 
     * @author      Subham Kharel
     * @version     1.1
     * @since       1.0
     * @see         {@link javax.swing.JOptionPane}
     */
	
	public Modal (String title, String message, int type)
	{
        showMessageDialog(null, message, title, type);
        
    }
}
