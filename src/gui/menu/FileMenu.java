/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.menu;

import gui.DataPanel;
import gui.PngIcon;
import logic.listener.menu.AddListener;
import logic.listener.menu.ExitListener;
import logic.listener.menu.ExportListener;
import logic.listener.menu.ImportListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

/**
 * {@link gui.menu.FileMenu} extends {@link javax.swing.JMenu}.
 * 
 * @author      Subham Kharel
 * 
 * @version     1.2
 * 
 * @since       1.0
 * 
 * @see         {@link javax.swing.JMenuItem}
 */

public class FileMenu extends JMenu
{
	private static final long serialVersionUID = 1L;
	
	protected JMenuItem newF, open, save, exit;
    protected DataPanel dataPanel;

    /**
	 * A constructor for {@link gui.menu.FileMenu}.
	 * It adds four menu item named:
	 *  <ul>
	 *  	<li>New</li>
	 *  	<li>Load</li>
	 *  	<li>Save</li>
	 *  	<li>Exit</li>
	 * </ul>
	 *  sets their icons and also sets the action listener class for them.
	 * 
	 * @param			dataPanel An instance of {@link gui.DataPanel}
	 * 
	 * @since       	1.0
	 * 
	 * @return			No return value.
	 * 
	 */
	
    public FileMenu (DataPanel dataPanel)
    {
        super("File");
        
        this.dataPanel = dataPanel;

        setIcon(new PngIcon("file"));

        newF = new JMenuItem("New");
        open = new JMenuItem("Load");
        save = new JMenuItem("Save");
        exit = new JMenuItem("Exit");
        

        newF.setIcon(new PngIcon("add"));
        open.setIcon(new PngIcon("open"));
        save.setIcon(new PngIcon("save"));
        exit.setIcon(new PngIcon("exit"));
        
        add(newF);
        add(open);
        add(save);
        add(exit);
        
        newF.addActionListener(new AddListener(dataPanel));
        open.addActionListener(new ImportListener(dataPanel));
        save.addActionListener(new ExportListener(dataPanel));
        exit.addActionListener(new ExitListener(dataPanel));
        
    }
}
