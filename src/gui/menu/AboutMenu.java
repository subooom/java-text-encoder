package gui.menu;

import gui.PngIcon;
import logic.listener.menu.HelpListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

/**
 * {@link gui.menu.AboutMenu} extends {@link javax.swing.JMenu}.
 * 
 * @author      Subham Kharel
 * @version     1.2
 * @since       1.0
 * @see         {@link javax.swing.JMenuItem}
 */

public class AboutMenu extends JMenu
{
	private static final long serialVersionUID = 1L;
	
	protected JMenuItem help;

	/**
	 * A constructor for {@link gui.menu.AboutMenu}.
	 * It adds one menu item named "Help" and also sets the action listener class for it.
	 * 
	 * @since       	1.0
	 * 
	 * @return			No return value.
	 * 
	 */
	
    public AboutMenu()
    {
        super("About");        
        
        setIcon(new PngIcon("about"));
        
        help = new JMenuItem("Help");
        
        help.setIcon(new PngIcon("help"));

        add(help);

        help.addActionListener(new HelpListener());
    }
}
