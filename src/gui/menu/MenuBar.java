/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.menu;

import gui.DataPanel;
import javax.swing.JMenuBar;

/**
 * {@link gui.menu.MenuBar} extends {@link javax.swing.JMenuBar}. Attach this to a JFrame.
 * 
 * @author      Subham Kharel
 * 
 * @version     1.2
 * 
 * @since       1.0
 * 
 * @see         {@link javax.swing.JMenuItem}
 */

public class MenuBar extends JMenuBar
{
	private static final long serialVersionUID = 1L;
	protected FileMenu file;
    protected AboutMenu about;

    /**
	 * A constructor for {@link gui.menu.FileMenu}.
	 * It adds two menus named:
	 *  <ul>
	 *  	<li>File</li>
	 *  	<li>About</li>
	 * </ul>
	 *  and also sets their icons.
	 * 
	 * @param			dataPanel An instance of {@link gui.DataPanel}
	 * 
	 * @since       	1.0
	 * 
	 * @return			No return value.
	 * 
	 */
	
    public MenuBar (DataPanel dataPanel)
    {
        super();
        
        file = new FileMenu(dataPanel);
        about = new AboutMenu();
        
        add(file);
        add(about);
    }
    
    
}
