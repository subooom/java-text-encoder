package gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

import logic.Coordinator;

public class Statistic extends JPanel 
{
	private static final long serialVersionUID = 1L;

	protected Graphics2D g2;
	
	protected Coordinator coordinator;
	
	protected int panelWidth = 570, panelHeight = 200;
	
	protected int xOffset = 17, yOffset = 40;
	
	protected double firstRectWidth = 0.0, secondRectWidth = 0.0;
	
	protected String type;
	
	protected float[] ratio = new float[2];
	
	public Statistic()
	{
		visible(false);
		setBackground(Color.white);
	}
	
	public void setup(int x, int y, String type)
	{	
		coordinator = new Coordinator(x, y, panelWidth, panelHeight, xOffset, panelWidth);

		ratio[0] = coordinator.calculatePercentage(x, x+y);
		ratio[1] = coordinator.calculatePercentage(y, x+y);
		
		if(x == 0 || y ==0) {
			this.firstRectWidth = 0;
			this.secondRectWidth = 0;
		}
		else {
			
			this.firstRectWidth = coordinator.getInputPixelsPerUnit();
			this.secondRectWidth = coordinator.getOutputPixelsPerUnit();
			this.type = type;
		}
	}
	
	public void visible(boolean value)
	{
		setVisible(value);
	}
	
	@Override
	public void paintComponent(Graphics g)
	{
		// This is called when the panel needs to be drawn to the screen
		
		// do default painting first (fills the background etc.)
		super.paintComponent(g);
		
		this.g2 = (Graphics2D) g;
		
		repaint();
		
		if(this.firstRectWidth == 0.0 || this.secondRectWidth == 0.0 || this.firstRectWidth == 0 || this.secondRectWidth == 0)
		{
			g2.setFont(new Font("Death Star", Font.PLAIN, 15));
			g2.drawString("- Please Encode or Decode a text file to generate the graph.", xOffset, yOffset+30);
		}
		// call the method that draws the graph.
		else
		{
			drawRecs();
			drawLabels();
			drawRatio();
			g2.dispose();
		}
	}
	
	private void drawRatio()
	{
		g2.setColor(new Color(89, 182, 59));
		g2.setFont(new Font("IMPACT", Font.BOLD, 15));
		g2.drawString(""+ratio[0], (int) xOffset, yOffset - 5);		

		g2.setColor(new Color(182,59,89));
		g2.setFont(new Font("IMPACT", Font.BOLD, 15));
		g2.drawString(""+ratio[1], (int) firstRectWidth + xOffset + 2, yOffset - 5);
	}

	private void drawRecs()
	{
		// First rect
		g2.setColor(new Color(89, 182, 59));
		g2.fillRect(xOffset, yOffset, (int) firstRectWidth, 400);
		
		// Divider line
		g2.setStroke(new BasicStroke(10));
		g2.setColor(Color.white);
		g2.drawLine((int) firstRectWidth + xOffset, yOffset, (int) firstRectWidth + xOffset, yOffset + panelHeight);

		// Second rect
		g2.setStroke(new BasicStroke(1));
		g2.setColor(new Color(182,59,89));
		g2.fillRect((int) firstRectWidth + xOffset, yOffset, (int) secondRectWidth, panelHeight);
	}
	
	private void drawLabels()
	{		
		if(this.type == "encoded")
		{
			// Title
			g2.setColor(new Color(182,59,89));
			g2.setFont(new Font("Death Star", Font.PLAIN, 40));
			g2.drawString("Space Complexity", panelWidth/2 - 100, yOffset - 5);
			
			// Labels
			g2.setColor(Color.white);
			g2.setFont(new Font("Death Star", Font.PLAIN, 10));
			g2.drawString("Y O U R   I N P U T", (int) xOffset + 5, panelHeight - 45);
		
			g2.setFont(new Font("Death Star", Font.PLAIN, 60));
			g2.drawString("M Y   O U T P U T", (int) firstRectWidth + 40, panelHeight - 45);
		}
		if(this.type == "decoded")
		{
			// Title
			g2.setColor(new Color(89, 182, 59));
			g2.setFont(new Font("Death Star", Font.PLAIN, 40));
			g2.drawString("Space Complexity", panelWidth/2 - 190, yOffset - 5);
			
			// Labels
			g2.setColor(Color.white);
			g2.setFont(new Font("Death Star", Font.PLAIN, 60));
			g2.drawString("Y O U R   I N P U T", (int) firstRectWidth/2 - 205, panelHeight - 45);
		
			g2.setFont(new Font("Death Star", Font.PLAIN, 10));
			g2.drawString("M Y   O U T P U T", (int) firstRectWidth + 25, panelHeight - 45);
		}
		
		
	}
}
