/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

/**
 * 
 * {@link gui.DataBoard} extends the {@link javax.swing.JScrollPane} making use
 * of inheritance.
 * We use {@link javax.swing.UIManager} to set a custom scrollbar {@link gui.CustomScrollBarUI}
 * to the ScrollPane {@link DataBoard}.
 * 
 * @author      Subham Kharel
 * @version     1.1
 * @since       1.0
 * @see         {@link javax.swing.JTextArea}, {@link javax.swing.JSrollPane}
 */

public class DataBoard extends JScrollPane
{
	private static final long serialVersionUID = 1L;
	
	protected JTextArea textarea;
	protected Color colorB, colorW;
	protected Font font;

	/**
     * A constructor for {@link gui.DataBoard}. It initializes a {@link javax.swing.JTextArea}
     * and fires up another member method <code>initialize()</code>.
     * 
     * @param ta   		a {@link javax.swing.JTextArea}
     * @param blue      blue color passed in from {@link gui.DataPanel}
     * @param white     white color passed in from {@link gui.DataPanel}
     * @param f		    font for the text in the JTextArea
     * 
     * 
     * @author      Subham Kharel
     * @version     1.1
     * @since       1.0
     * @see         {@link javax.swing.UIManager}, {@link javax.swing.JTextArea}, {@link javax.swing.JScrollPane},
     * 				{@link java.awt.Color}, {@link java.awt.Font}
     */
	
	public DataBoard(JTextArea ta, Color white, Color blue, Font f)
	{
		super(ta);
		
		textarea = ta;
		colorW = white;
		colorB = blue;
		font = f;
		
		textarea.setMargin(new Insets(20, 10, 0, 20));

		initialize();
	}

	/**
     * This initializes the vital steps needed to set the viewport of the JScrollPane to the JTextArea.
     * 
     * 
     * @see         {@link java.awt.Color}
     * @return		Current instance of {@link gui.DataBoard}
     * @since       1.0
     */
	
	public DataBoard initialize()
	{
		textarea.setVisible(true);
		textarea.setFont(font);

		textarea.setBackground(colorB);
		textarea.setForeground(colorW);

		getVerticalScrollBar().setUI(new CustomScrollBarUI());

				
		setViewportView(textarea);

		setBorder(null);

		setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

		visible(false);

		return this;
	}

	/**
     * This takes in a boolean value and sets the visibility of the {@link gui.DataBoard}.
     * 
     * @param visible	visibility of the DataBoard
     * 
     * @see         	{@link javax.swing.JScrollPane}
     * @return			Current instance of DataBoard
     * @since   	    1.0
     */
	
	public DataBoard visible(boolean visible)
	{
		
		setVisible(visible);

		return this;
	}

	/**
     * This takes in a String value and sets the text of the JTextArea.
     * 
     * @param text		Content to be stored in JTextArea
     * 
     * @see         	{@link javax.swing.JTextArea}
     * @return			Current instance of DataBoard
     * @since   	    1.0
     */
	
	public DataBoard text(String text)
	{
		
		textarea.setText(text);

		return this;
	}
	
	/**
     * This is a getter method to get the contents of the JTextArea.
     * 
     * 
     * @see         	{@link javax.swing.JTextArea}
     * @return			Contents of the textarea
     * @since   	    1.0
     */
	
	public String getText()
	{
		
		return textarea.getText();
		
	}

	/**
     * This checks is the JTextArea has text or not. Returns true if there is text.
     * 
     * 
     * @see         {@link java.awt.Color}
     * @return		<code>true</code> if there is text in the TextArea
     * @since       1.0
     */
	
	public boolean hasText()
	{
		
		return (getText().length() == 0) ? false : true;
		
	}

	/**
     * This is the getter method to get the JTextArea for ease of use.
     * 
     * 
     * @see         {@link javax.swing.JTextArea}
     * @return		current instance of JTextArea
     * @since       1.0
     */
	
	public JTextArea getTextarea()
	{
		return this.textarea;
	}
}
