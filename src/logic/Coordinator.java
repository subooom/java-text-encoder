package logic;

public class Coordinator
{	
	public final static double UNIT = 20;
	
	protected final static double EPSILON = 1e-12;
	
	protected int width, height, offset, panelWidth;
	protected double inputPixelsPerUnit, outputPixelsPerUnit;
	
	protected float totalLength;
	protected float inputPercentage, outputPercentage;
	
	public Coordinator(int inputLength, int outputLength, int width, int height, int offset, int panelWidth)
	{		
		this.totalLength = inputLength + outputLength;
		
		this.width = width-offset;
		this.height = height;
		
		this.offset = offset;
		
		this.panelWidth = panelWidth;

		inputPercentage = inputLength; // temporarily storing it in this var only to modify this with the real percentage later on
		outputPercentage = outputLength;
		
	}
	
	private void generatePixelsPerUnit()
	{
		this.inputPixelsPerUnit = map(inputPercentage, 0, 100, offset, width - offset);
		this.outputPixelsPerUnit = map(outputPercentage, 0, 100, offset, width - offset);
	}
	
	public double map(double value, double start, double end, double minRange, double maxRange)
	{
	    if (Math.abs(end - start) < EPSILON)
	    {
	        throw new ArithmeticException("/ 0");
	    }

	    double offset = minRange;
	    
	    double ratio = (maxRange - minRange) / (end - start);
	    return ratio * (value - start) + offset;
	}
	
	public float calculatePercentage(int x, float total)
	{
		return x/total*100;
	}
	
	protected void generate()
	{
		inputPercentage = calculatePercentage((int)inputPercentage, totalLength);
		outputPercentage = calculatePercentage((int)outputPercentage, totalLength);
		
		generatePixelsPerUnit();
	}
	
	public double getInputPixelsPerUnit()
	{
		generate();
		
		return inputPixelsPerUnit;
		
	}
	
	public double getOutputPixelsPerUnit()
	{
		
		return outputPixelsPerUnit;
		
	}
}
