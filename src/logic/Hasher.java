package logic;

import gui.DataBoard;
import logic.interfaces.Hashing;

/**
 * {@link gui.Hasher} implements the {@link gui.interface.Hashing} interface
 * and so has all the said methods and a constructor.
 * The roughly sketched algorithm is:
 * <ol>
 * 	<li>Checksum</li>
 * 	<li>Rotate</li>
 * 	<li>Checksum+fibbonaciNumbers</li>
 * </ol>
 * 
 * @author      Subham Kharel
 * @version     1.3
 * @since       1.3
 */

public class Hasher implements Hashing{
	protected final int[] fibbonaciNumbers = {2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987};
	protected String rawText;
	protected String hash;
	
   /**
    * A constructor for class {@link logic.Hasher}.
    * 
    * @param		db An instance of {@linkplain gui.DataBoard}.
    * 
    * @return       No return value
    * @see          {@link logic.Encoder}, {@link logic.Decoder}
    * @since        1.3
    */
	
	public Hasher(DataBoard db)
	{
		rawText = db.getText();
		
		hash = checksum(rawText.getBytes());
		
	}

	/**
     * This is the root and soul of {@linkplain logic.Hasher} which does all the
     * good work. This method is called when {@link java.awt.event.MouseListener}:clickevent
     * is fired on the {@code generate hash} JButton.
     * 
     * @param		db An instance of {@link gui.DataBoard}
     * 
     * @see         {@linkplain logic.Decoder}, {@link gui.ButtonGroup}
     * @return		str Checksum output
     * @since       1.2
     */
	
	@Override
	public String checksum(byte[] input)
	{
		int hass = 0;
	    byte checksum = 0;
	    int index = 0;
	    
	    for (byte cur_byte: input)
	    {
	        checksum = (byte) (((checksum & 0xFF) >>> 1) + ((checksum & 0x1) << 7)); // Rotate the accumulator
	        checksum = (byte) ((checksum + cur_byte) & 0xFF);   // Add the next chunk
	        
	        hass = hass + checksum*fibbonaciNumbers[index];
	        index++;
	        if(index == fibbonaciNumbers.length-1) 
	        {
	        	index = 0;
	        }
	    }
	    return ""+hass;
	}

	
   /**
     * This returns the hashed value after everything finishes successfully.
     * 
     * 
     * @return		encodedText The encoded output.
     * @since       1.2
     */
	@Override
	public String getHashedValue()
	{
		return hash;
	}

}
