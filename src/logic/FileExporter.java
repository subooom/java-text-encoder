package logic;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import gui.DataPanel;
import gui.Modal;
import logic.interfaces.FileExporting;

/**
 * {@link logic.FileExporter} implements the {@link gui.interface.FileExporting} interface
 * 
 * This is a JFileChooser which saves to a user desired text file.
 * 
 * @author      Subham Kharel
 * @version     1.3
 * @since       1.2
 */

public class FileExporter implements FileExporting
{	
    /**
     * This opens up a JFileOpener with an FileNameExtensionFilter so that only text files
     * can be added.
     * 
     * @param 		dataPanel
     * 
     * @see         {@link java.awt.Graphics}
     * @return		No return value.
     * @since       1.2
     */
	
	@Override
	public void open(DataPanel dataPanel)
	{
		if(dataPanel.getDataBoard().hasText())
		{
			JFileChooser chooser = new JFileChooser();
			FileNameExtensionFilter filter = new FileNameExtensionFilter(".txt", "txt", "text");
			chooser.setFileFilter(filter);
			int status = chooser.showSaveDialog(null);
			if (status == JFileChooser.APPROVE_OPTION)
			{
				File file = chooser.getSelectedFile();
				try
				{
					FileWriter fw = new FileWriter(file+".txt");
					String text = dataPanel.getDataBoard().getText();
					fw.write(text);
					dataPanel.getDataBoard().text(text);
					fw.close();
				}
				catch (IOException e)
				{
//					       do nothing
				}
			}
    	 }
    	 else
    	 {
    		 new Modal("Error", "No text to export.", JOptionPane.ERROR_MESSAGE);
    	 }
	}
}
