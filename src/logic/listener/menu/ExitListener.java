package logic.listener.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import gui.DataPanel;
import logic.FileImporter;

/**
 * {@link logic.listener.menu.ExitListener} implements the MouseListener interface
 * and so has all the said methods and a few constructors.
 * 
 * @author      Subham Kharel
 * @version     1.3
 * @since       1.2
 * @see         {@link logic.Decoder}
 */

public class ExitListener implements ActionListener
{

	protected DataPanel dataPanel;
	protected FileImporter fileChooser = new FileImporter();
		
   /**
     * This is the constructor method.
     * 
     * @param 		dp {@linkplain gui.DataPanel}
     * 
     * @return		No return value.
     * @since       1.2
     */
	
	public ExitListener(DataPanel dp)
	{
		this.dataPanel = dp;
	}

	/**
     * These is the event fired on click.
     * 
     * @param 		e MouseEvent
     * 
     * @return		No return value.
     * @since       1.2
     */
	
	
    @Override
    public void actionPerformed(ActionEvent ae) {
        int dialogButton = JOptionPane.YES_NO_OPTION;

        int dialogResult = JOptionPane.showConfirmDialog(
                null, "Are you sure?", "Warning", dialogButton, JOptionPane.ERROR_MESSAGE
        );

        if (dialogResult == JOptionPane.YES_OPTION) {
            System.exit(0);
        }
//    else {
//        do nothing
//    }
    }

}