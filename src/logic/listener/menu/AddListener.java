package logic.listener.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import gui.DataPanel;

/**
 * {@link logic.listener.menu.AddListener} implements the MouseListener interface
 * and so has all the said methods and a few constructors.
 * 
 * @author      Subham Kharel
 * @version     1.3
 * @since       1.2
 * @see         {@link logic.Decoder}
 */

public class AddListener implements ActionListener
{
	
	protected DataPanel dataPanel;
	 
    /**
     * This is the constructor method.
     * 
     * @param 		dp {@linkplain gui.DataPanel}
     * 
     * @return		No return value.
     * @since       1.2
     */
	
	public AddListener(DataPanel dp)
	{
		dataPanel = dp;
	}
	
	/**
     * These is the event fired on click.
     * 
     * @param 		e MouseEvent
     * 
     * @return		No return value.
     * @since       1.2
     */
	
	@Override
	public void actionPerformed(ActionEvent ae)
	{
		this.dataPanel.createNewFile();
	}

}
