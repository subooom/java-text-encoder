package logic.listener.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import gui.Modal;

/**
 * {@link logic.listener.menu.HelpListener} implements the MouseListener interface
 * and so has all the said methods and a few constructors.
 * 
 * @author      Subham Kharel
 * @version     1.3
 * @since       1.2
 * @see         {@link logic.Decoder}
 */

public class HelpListener implements ActionListener {
	
	/**
     * These is the event fired on click.
     * 
     * @param 		e MouseEvent
     * 
     * @return		No return value.
     * @since       1.2
     */
	
    @Override
    public void actionPerformed(ActionEvent ae) {            
        new Modal("User Guide",
                "This is a text encoding/decoding and a hash generating toolkit.\n"
                + "Import a text file or create a new one yourself to begin.\n\n"
                + "Developed By Subham Kharel."
                , JOptionPane.PLAIN_MESSAGE);
    }
    
}