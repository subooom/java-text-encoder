package logic.listener.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import gui.DataPanel;
import logic.FileExporter;

/**
 * {@link logic.listener.menu.ExportListener} implements the MouseListener interface
 * and so has all the said methods and a few constructors.
 * 
 * @author      Subham Kharel
 * @version     1.3
 * @since       1.2
 * @see         {@link logic.FileExporter}
 */

public class ExportListener implements ActionListener
{ 
	protected DataPanel dataPanel;
	protected FileExporter fileSaver = new FileExporter();
	
   /**
     * This is the constructor method.
     * 
     * @param 		dp {@linkplain gui.DataPanel}
     * 
     * @return		No return value.
     * @since       1.2
     */
	
	public ExportListener(DataPanel dp)
	{
		this.dataPanel = dp;
	}
	
	/**
     * These is the event fired on click.
     * 
     * @param 		e MouseEvent
     * 
     * @return		No return value.
     * @since       1.2
     */
	
	 @Override
     public void actionPerformed(ActionEvent ae)
	 {
		 fileSaver.open(dataPanel);
	 }
}