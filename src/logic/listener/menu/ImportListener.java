package logic.listener.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import gui.DataPanel;
import logic.FileImporter;

/**
 * {@link logic.listener.menu.ImportListener} implements the MouseListener interface
 * and so has all the said methods and a few constructors.
 * 
 * @author      Subham Kharel
 * @version     1.3
 * @since       1.2
 * @see         {@link logic.FileImporter}
 */

public class ImportListener implements ActionListener
{

    protected DataPanel datapanel;
    protected FileImporter fileChooser = new FileImporter();
	
    /**
      * This is the constructor method.
      * 
      * @param 		dp {@linkplain gui.DataPanel}
      * 
      * @return		No return value.
      * @since       1.2
      */
 	
    public ImportListener(DataPanel dp)
    {
    	this.datapanel = dp;
    }

	/**
     * These is the event fired on click.
     * 
     * @param 		e MouseEvent
     * 
     * @return		No return value.
     * @since       1.2
     */
	
    public void actionPerformed(ActionEvent ae)
    {
        fileChooser.open(datapanel);
    }
}
