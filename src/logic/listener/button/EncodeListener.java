package logic.listener.button;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;

import gui.DataBoard;
import gui.DataPanel;
import logic.Decoder;
import logic.Encoder;
import logic.FileImporter;

/**
 * {@link logic.listener.button.EncodeListener} implements the MouseListener interface
 * and so has all the said methods and a few constructors.
 * 
 * @author      Subham Kharel
 * @version     1.3
 * @since       1.2
 * @see         {@link logic.Decoder}
 */

public class EncodeListener implements MouseListener
{	
	protected JButton encode;
    protected Encoder encoder;
    protected Decoder decoder;
    protected DataBoard dataBoard;
    protected DataPanel dataPanel;
    protected FileImporter fileChooser;
    
    /**
     * This isthe constructor method.
     * 
     * @param 		enc J{@linkplain javax.swing.JButton}
     * @param 		db {@linkplain gui.DataBoard}
     * @param 		dp {@linkplain gui.DataPanel}
     * @param 		encoder {@linkplain logic.Encoder}
     * @param 		decoder {@linkplain logic.Decoder}
     * 
     * @return		No return value.
     * @since       1.2
     */
    
	public EncodeListener(JButton enc, DataBoard db, DataPanel dp, Encoder encoder, Decoder decoder)
	{
		this.encode = enc;
		this.dataBoard = db;
		this.dataPanel = dp;
		this.encoder = encoder;
		this.decoder = decoder;
	}

    /**
     * These is the event fired on click.
     * 
     * @param 		e MouseEvent
     * 
     * @return		No return value.
     * @since       1.2
     */
    
	@Override
	public void mouseClicked(MouseEvent e)
	{
		
    	if(!dataBoard.hasText())
    	{
    		fileChooser = new FileImporter();
    		fileChooser.open(dataPanel);    		
    	}

    	if(dataBoard.hasText())
    	{
        	encoder.encode(dataPanel);
    	}
	}

	@Override
	public void mouseEntered(MouseEvent e)
	{
		encode.setBackground(Color.decode("#4765c3"));		
	}

	@Override
	public void mouseExited(MouseEvent e)
	{
		encode.setBackground(Color.decode("#3b59b6"));		
	}

	@Override
	public void mousePressed(MouseEvent e)
	{
	}

	@Override
	public void mouseReleased(MouseEvent e)
	{
		
		
	}


}
