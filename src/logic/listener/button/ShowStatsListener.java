package logic.listener.button;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import gui.Button;
import gui.DataBoard;
import gui.DataPanel;
import gui.Statistic;
import logic.FileImporter;

public class ShowStatsListener implements MouseListener
{	
	protected Button genHash;
	protected Statistic statObj;
	protected DataPanel dataPanel;
	protected DataBoard dataBoard;
	protected FileImporter fileChooser;
	
	public ShowStatsListener(Statistic stats, DataPanel dp, Button gh)
	{	
		this.statObj = stats;
		this.dataPanel = dp;
		this.genHash = gh;
		
		this.dataBoard = dp.getDataBoard();
	}
	
	@Override
	public void mouseClicked(MouseEvent e)
	{
		statObj.visible(true);
		dataPanel.setTitleVisibility(false);
		if(dataBoard.hasText()) statObj.setup(dataPanel.getInputLength(), dataPanel.getOutputLength(), dataPanel.getOutputMethod());		
	}

	@Override
	public void mouseEntered(MouseEvent e)
	{
		
	}

	@Override
	public void mouseExited(MouseEvent e)
	{
		
	}

	@Override
	public void mousePressed(MouseEvent e)
	{
		
	}

	@Override
	public void mouseReleased(MouseEvent e)
	{
		
	}

}
