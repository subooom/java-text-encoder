package logic.listener.button;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;

import gui.DataBoard;
import gui.DataPanel;
import logic.Decoder;
import logic.Encoder;
import logic.FileImporter;

/**
 * {@link logic.listener.button.DecodeListener} implements the MouseListener interface
 * and so has all the said methods and a few constructors.
 * 
 * @author      Subham Kharel
 * @version     1.3
 * @since       1.2
 * @see         {@link logic.Decoder}
 */
public class DecodeListener implements MouseListener
{
	protected JButton decode;
    
	protected Decoder decoder;
    protected Encoder encoder;
    
    protected DataBoard dataBoard;
    protected DataPanel dataPanel;
    
    protected FileImporter fileChooser;
        
  	/**
    * This isthe constructor method.
    * 
    * @param 		dec J{@linkplain javax.swing.JButton}
    * @param 		db {@linkplain gui.DataBoard}
    * @param 		dp {@linkplain gui.DataPanel}
    * @param 		encoder {@linkplain logic.Encoder}
    * @param 		decoder {@linkplain logic.Decoder}
    * 
    * @return		No return value.
    * @since       1.2
    */
    
    public DecodeListener(JButton dec, DataBoard db, DataPanel dp, Decoder decoder, Encoder encoder)
    {
		this.decode = dec;
		this.dataBoard = db;
		this.dataPanel = dp;
		this.decoder = decoder;
		this.encoder = encoder;
	}
    
    /**
     * These is the event fired on click.
     * 
     * @param 		e MouseEvent
     * 
     * @return		No return value.
     * @since       1.2
     */
    
	@Override
	public void mouseClicked(MouseEvent e)
	{
		if(!dataBoard.hasText())
		{
    		fileChooser = new FileImporter();
    		fileChooser.open(dataPanel);
    	}
		if(dataBoard.hasText())
		{
	    	decoder.decode(dataPanel);
		}
	}

	@Override
	public void mouseEntered(MouseEvent e)
	{
		decode.setBackground(new Color(110,199,81));		
	}

	@Override
	public void mouseExited(MouseEvent e)
	{
    	decode.setBackground(new Color(89, 182, 59));		
	}

	@Override
	public void mousePressed(MouseEvent e)
	{
		
	}

	@Override
	public void mouseReleased(MouseEvent e)
	{
		
	}

}
