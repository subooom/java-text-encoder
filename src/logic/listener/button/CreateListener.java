package logic.listener.button;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;

import gui.DataPanel;
import logic.FileImporter;

/**
 * {@link logic.listener.button.CreateListener} implements the MouseListener interface
 * and so has all the said methods and a few constructors.
 * 
 * @author      Subham Kharel
 * @version     1.3
 * @since       1.2
 * @see         {@link logic.Decoder}
 */

public class CreateListener implements MouseListener
{
	protected JButton create;
    protected DataPanel dataPanel;
    protected FileImporter fileChooser;
    
   	/**
    * These two are the constructor methods.
    * 
    * @param 		create J{@linkplain javax.swing.JButton}
    * @param 		dp {@linkplain gui.DataPanel}
    * 
    * @return		No return value.
    * @since       1.2
    */
    public CreateListener(JButton create, DataPanel dp)
    {
		this.create = create;
		this.dataPanel = dp;
	}
    public CreateListener(DataPanel dp)
    {
		this.dataPanel = dp;
	}

	/**
     * These is the event fired on click.
     * 
     * @param 		e MouseEvent
     * 
     * @return		No return value.
     * @since       1.2
     */
    
	@Override
	public void mouseClicked(MouseEvent e)
	{
		this.dataPanel.createNewFile();
	}
		
	@Override
	public void mouseEntered(MouseEvent e)
	{
		create.setBackground(new Color(81, 199, 170));	
	}

	@Override
	public void mouseExited(MouseEvent e)
	{
		create.setBackground(new Color(59, 182, 152));
	}

	@Override
	public void mousePressed(MouseEvent e)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e)
	{
		// TODO Auto-generated method stub
		
	}

}
