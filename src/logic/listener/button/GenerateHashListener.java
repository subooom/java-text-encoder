package logic.listener.button;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;

import gui.DataBoard;
import gui.DataPanel;
import gui.Statistic;
import logic.FileImporter;
import logic.Hasher;

/**
 * {@link logic.listener.button.GenerateHashListener} implements the MouseListener interface
 * and so has all the said methods and a few constructors.
 * 
 * @author      Subham Kharel
 * @version     1.3
 * @since       1.2
 * @see         {@link logic.GenerateHash}
 */

public class GenerateHashListener  implements MouseListener
{

	protected JButton generateHash;
    protected Hasher hasher;
    protected DataBoard dataBoard;
    protected DataPanel dataPanel;
    protected FileImporter fileChooser;
    protected Statistic statObj;
    
    /**
     * This isthe constructor method.
     * 
     * @param 		gen J{@linkplain javax.swing.JButton}
     * @param 		db {@linkplain gui.DataBoard}
     * @param 		dp {@linkplain gui.DataPanel}
     * 
     * @return		No return value.
     * @since       1.2
     */
    
    public GenerateHashListener(JButton gen, DataBoard db, DataPanel dp, Statistic stat)
    {
    	this.statObj = stat;
		this.generateHash = gen;
		this.dataBoard = db;
		this.dataPanel = dp;
    }

    /**
     * These is the event fired on click.
     * 
     * @param 		e MouseEvent
     * 
     * @return		No return value.
     * @since       1.2
     */
    
	@Override
	public void mouseClicked(MouseEvent e)
	{
		if(!dataBoard.hasText())
		{
    		fileChooser = new FileImporter();
    		fileChooser.open(dataPanel);
    		
    	}
		if(dataBoard.hasText()){
			this.hasher= new Hasher(this.dataBoard);
			statObj.setVisible(false);
			dataPanel.setTitleVisibility(true);
			dataPanel.setTitleText("Hash: "+hasher.getHashedValue());
		}
	}

	@Override
	public void mouseEntered(MouseEvent e)
	{
    	generateHash.setBackground(new Color(199, 111, 81));
		
	}

	@Override
	public void mouseExited(MouseEvent e)
	{
    	generateHash.setBackground(new Color(182, 90, 59));
		
	}

	@Override
	public void mousePressed(MouseEvent e)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e)
	{
		// TODO Auto-generated method stub
		
	}

}
