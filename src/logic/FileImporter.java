/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic;

import gui.DataPanel;
import gui.Modal;

import logic.interfaces.FileImporting;

import java.io.FileNotFoundException;
import java.util.Scanner;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.io.File;

/**
 * {@link logic.FileImporter} implements the {@link gui.interface.FileImporting} interface
 * 
 * This is a JFileChooser
 * 
 * @author      Subham Kharel
 * @version     1.3
 * @since       1.1
 */

public class FileImporter implements FileImporting
{    
    protected Scanner scanner;
	protected String fileName;
    protected JFileChooser fileChooser = new JFileChooser();
    
    /**
     * This opens up a JFileOpener with an FileNameExtensionFilter so that only text files
     * can be added.
     * 
     * @param 		dataPanel
     * 
     * @see         {@link java.awt.Graphics}
     * @return		No return value.
     * @since       1.1
     */
    
    @Override
    public void open(DataPanel dataPanel)
    {
    	fileChooser.setDialogTitle("Open a text file");
    	FileNameExtensionFilter filter = new FileNameExtensionFilter(".txt", "txt", "text");
    	fileChooser.setFileFilter(filter);
        int approved = fileChooser.showOpenDialog(null);

            if (approved != JFileChooser.APPROVE_OPTION)
            {
                
                new Modal("Error", "No File Chosen!", JOptionPane.ERROR_MESSAGE);

                return;
            }

            File file = fileChooser.getSelectedFile();
            fileName = file.getName();
            
            try
            {
                scanner = new Scanner(file);
                String content = "";

                while (scanner.hasNext())
                {
                    content += scanner.nextLine();
                }
                dataPanel.storeData(content);
                dataPanel.setTitleText(fileName);
                
            }
            catch (FileNotFoundException e1)
            {
                new Modal("Error", "File Not Found!", JOptionPane.ERROR_MESSAGE);
            }
    }
}
