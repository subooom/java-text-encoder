package logic;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import gui.DataBoard;
import gui.DataPanel;
import logic.interfaces.Encryption;

/**
 * {@link gui.Encoder} implements the {@link gui.interface.Encryption} interface
 * and so has all the said methods and a constructor.
 * The roughly sketched algorithm is:
 * <ol>
 * 	<li>Binary generation</li>
 * 	<li>Compression</li>
 * 	<li>Mapping to Unique Characters</li>
 * </ol>
 * 
 * @author      Subham Kharel
 * @version     1.3
 * @since       1.2
 * @see         {@link logic.Decoder}
 */

public class Encoder implements Encryption
{
							        // format for   0    1    2    3    4    5    6    7   delimeter
	public static final char[] ENCRYPTIONFORMAT = {'A', '!', '$', '@', '^', 'p', 'e', 'a', 's'};
	
	protected String rawText, encodedText = "", mapped = "", compressed = "";
	protected String delimeter = " ";
	protected String[] byteArray;
	protected int maxLength;

	/**
     * This method prepares the text for future puproses essentially by saving the text in an attribute.
     * It also initializes the byteArray.
     * 
     * @param 		text The text content supplied for decoding
     * 
     * @return		No return value.
     * @since       1.2
     */
	
	@Override
	public void setText(String text)
	{
		rawText = text;
		byteArray = new String[text.length()];
	}

	/**
     * This is the root and soul of {@linkplain logic.Encoder} which does all the
     * good work. This method is called when {@link java.awt.event.MouseListener}:clickevent
     * is fired on the {@code encode} JButton.
     * 
     * @param		d An instance of {@link gui.DataPanel}
     * 
     * @see         {@linkplain logic.Decoder}, {@link gui.ButtonGroup}
     * @return		No return value.
     * @since       1.2
     */
	
	@Override
	public String encode(DataPanel dp)
	{	
		DataBoard db = dp.getDataBoard();
		
		nullifyVariables();
		setText(db.getText());		
		
		for(int i = 0; i<rawText.length(); i++)
		{
			// get current byte                                                   
			String currentByte = generateByteAt(rawText, i);
			compressed += compressString(currentByte)+delimeter;
		}
		for(int j = 0; j<compressed.length(); j++)
		{
			mapped += mapToUniqueCharacters(compressed.charAt(j), j);
		}

		encodedText = mapped;
		
		db.text(encodedText);
		
		dp.setInputLength(rawText.length()); // For statistic
		dp.setOutputLength(encodedText.length()); // For statistic		
		dp.setOutputMethod("encoded"); // For statistic
		
		return encodedText;
	}
	
	@Override
	public void nullifyVariables()
	{
		mapped = "";
		compressed = "";
		encodedText = "";
	}
	
	/**
     * {@code mapToUniqueCharacters} simply maps the `compressed.charAt(j)` based on a static final variable
     * {@link logic.Encoder.ENCRYPTION_FORMAT} which is the first step done while decoding in this algorithm.
     * 
     * 
     * @param		c 		A character to unmap
     * @param		index	index of the current iteration
     * 
     * @see         {@linkplain logic.Decoder}
     * @return		mapped		The mapped value
     * @since       1.2
     */
	
	@Override
	public String mapToUniqueCharacters(char c, int index)
	{
		String temp = "", mapped = "";
		switch(c) {
		case '0':
			temp += ""+ENCRYPTIONFORMAT[0];
			break;
		case '1':
			temp += ""+ENCRYPTIONFORMAT[1];
			break;
		case '2':
			temp += ""+ENCRYPTIONFORMAT[2];
			break;
		case '3':
			temp += ""+ENCRYPTIONFORMAT[3];
			break;
		case ' ':
			temp += ""+ENCRYPTIONFORMAT[8];
			break;
		case '4':
			temp += ""+ENCRYPTIONFORMAT[4];
			break;
		case '5':
			temp += ""+ENCRYPTIONFORMAT[5];
			break;
		case '6':
			temp += ""+ENCRYPTIONFORMAT[6];
			break;
		case '7':
			temp += ""+ENCRYPTIONFORMAT[7];
			break;
		default:
			temp+=""+c;
			break;
		}
		mapped += temp;
		
		return mapped;
	}


	/**
     * {@code generateByteAt} simply generates the `rawText` 's equivalent of byte and stores it in an array.
     * {@link logic.Encoder.ENCRYPTION_FORMAT} which is the first step done while decoding in this algorithm.
     * 
     * 
     * @param		str				A character to unmap
     * @param		i	 			index of the current iteration
     * 
     * @see         {@linkplain logic.Decoder}
     * 
     * @return		byteArray[i]		The byte array
     * @since       1.2
     */
	
	@Override
	public String generateByteAt(String str, int i)
	{
		byteArray[i] = Integer.toString((int)str.charAt(i),2) +" ";
		
		return byteArray[i];
	}
	

	/**
     * {@code compressString} uses {@linkplain java.util.regex.Pattern} and its quite handy methods to compress
     * the string. I make pretty good use of the  {@link java.util.regex.Matcher} class as well, using it's group()
     * method. *grins*<br />
     * 
     * 
     * @param		str A string to compress
     * 
     * @see         {@linkplain logic.Encoder}
     * @return		compressed	Compressed output
     * @since       1.2
     */
	@Override
	public String compressString(String str)
	{
		String compressed = "";
	    Pattern pattern = Pattern.compile("([\\w])\\1*");
	    Matcher matcher = pattern.matcher(str);
	    while (matcher.find())
	    {
	        String group = matcher.group();
	        if (group.length() > 1) compressed += group.length() + "";
	        compressed += group.charAt(0);
	    }
	    return compressed;
   }
	
   /**
     * This returns the encoded text after everything finishes successfully.
     * 
     * 
     * @see         {@link java.awt.Graphics}
     * @return		encodedText The encoded output.
     * @since       1.2
     */
	
	@Override
	public String getEncodedText()
	{
		return encodedText;
	}
}
