package logic;

import java.util.Collections;

import javax.swing.JOptionPane;

import gui.DataBoard;
import gui.DataPanel;
import gui.Modal;
import logic.interfaces.Decryption;

/**
 * {@link gui.Decoder} implements the {@link gui.interfaces.Decryption} interface
 * and so has all the said methods and a constructor.
 * The roughly sketched algorithm is:
 * <ol>
 * 	<li>Unmapping from Unique Characters</li>
 * 	<li>Decompression</li>
 * 	<li>ASCII generation</li>
 * </ol>
 * 
 * @author      Subham Kharel
 * @version     1.3
 * @since       1.1
 * @see         {@link logic.Encoder}
 */

public class Decoder implements Decryption
{
	protected String rawText, decodedText = "", unmapped = "", decompressed = "";
	protected String delimeter = " ";
	
	protected String[] asciiArray;
	protected int maxLength;
    
//    protected TwoBitScore score;

    /**
     * This method prepares the text for future puproses essentially by saving the text in an attribute.
     * 
     * @param 		text The text content supplied for decoding
     * 
     * @return		No return value.
     * @since       1.2
     */
	
	@Override
	public void setText(String text)
	{
		this.rawText = text;
	}
	
	/**
     * This is practically the main method of {@linkplain logic.Decoder} which does all the
     * good work. This method is called when {@link java.awt.event.MouseListener}:clickevent is fired on the {@code decode} JButton.
     * 
     * @param		db An instance of {@link gui.DataBoard}
     * 
     * @see         {@linkplain logic.Decoder}, {@link gui.ButtonGroup}
     * @return		No return value.
     * @since       1.2
     */
	
	@Override
	public String decode(DataPanel dp)
	{	
		DataBoard db = dp.getDataBoard();
		
		// For the next call, we flush all our variables in use
		nullifyVariables();
		
		// Gets the text from the JTextArea and sets it to be equal to {@code rawText}
		setText(db.getText());
		
		// Algorithm
		try
		{
			for(int i = 0; i<rawText.length(); i++)
			{
				// Unmap First (Gets the i-th character of rawText and sends it)
				unMapFromUniqueCharacters(rawText.charAt(i));
				
			}
			
			// Decompress Second (sends in the unmapped data)
			decompressed = decompressString(unmapped);
			
			//ASCII third (sends in the decompressed data)
			decodedText = convertToAscii(decompressed);
			
		}
		// End of algorithm
		catch(Exception e)
		{
			// Show a JOptionPane
			new Modal("Error", "The text is not encoded or wrongly encoded.", JOptionPane.ERROR_MESSAGE);
		}
		
		// Update the text on the JTextArea with the decoded text
		db.text(decodedText);

		dp.setInputLength(rawText.length()); // For statistic
		dp.setOutputLength(decodedText.length()); // For statistic		
		dp.setOutputMethod("decoded"); // For statistic
		
		return decodedText;
		
	}
	
	/**
     * {@code unMapFromUniqueCharacters} simply unmaps the `rawText` based on a static final variable
     * {@link logic.Encoder.ENCRYPTION_FORMAT} which is the last step done while encoding in this algorithm.
     * 
     * I've just added a bunch of if-statements to check for the matching character which I'm pretty sure could be
     * re-written in a much more beautiful manner while also taking care of the redundencies in code, but this is
     * what I'm going with for now.
     * 
     * @param		c A character to unmap
     * 
     * @see         {@linkplain logic.Encoder}
     * @return		No return value.
     * @since       1.2
     */

	@Override
	public void unMapFromUniqueCharacters(char c)
	{	
		String temp = "";
		
		// Bunch of if-statements
		if(c == Encoder.ENCRYPTIONFORMAT[0])
		{
			temp+=""+0;
		}
		else if(c == Encoder.ENCRYPTIONFORMAT[1])
		{
			temp+=""+1;
		}
		else if(c == Encoder.ENCRYPTIONFORMAT[2])
		{
			temp+=""+2;
		}
		else if(c == Encoder.ENCRYPTIONFORMAT[3])
		{
			temp+=""+3;
		}
		else if(c == Encoder.ENCRYPTIONFORMAT[4])
		{
			temp+=""+4;
		}
		else if(c == Encoder.ENCRYPTIONFORMAT[5])
		{
			temp+=""+5;
		}
		else if(c == Encoder.ENCRYPTIONFORMAT[6])
		{
			temp+=""+6;
		}
		else if(c == Encoder.ENCRYPTIONFORMAT[7])
		{
			temp+=""+7;			
		}
		else if(c == Encoder.ENCRYPTIONFORMAT[8])
		{
			temp+=" ";			
		}
		
		// Append the current temp into `unmapped`
		this.unmapped += temp;
		
		// Returns nothing apparently, this could have easily been managed better for no can do.
		
	}


	/**
     * {@code decompressString} uses {@linkplain java.lang.StringBuilder} and it's quite handy methods to decompress
     * the string. I make pretty good use of the  {@link java.util.Collections} class as well, using it's ncopies()
     * method. *grins*<br />
     * 
     * I've just added a bunch of if-statements to check for the matching character which I'm pretty sure could be
     * re-written in a much more beautiful manner while also taking care of the redundencies in code, but this is
     * what I'm going with for now.
     * 
     * @param		str A character to unmap
     * 
     * @see         {@linkplain logic.Encoder}
     * @return		No return value.
     * @since       1.2
     */
	
	@Override
	public String decompressString(String str)
	{
		StringBuilder s = new StringBuilder();
		
		// Another for loop as if one was not enough, there's a reason why this algorithm is called two-bit encryption
	    for (int i = 0; i < str.length(); i++)
	    {
	        char c = str.charAt(i);
	        if(c == '1')
	        {
	        	s.append("1");
	        }
	        if(c == '0')
	        {
	        	s.append("0");
	        }
	        if (c == ' ')
	        {
	            s.append(' ');
	        }
	        else if(c == '2')
	        {
	            s.append(String.join("", Collections.nCopies(2, ("" + str.charAt(++i)) ) ));
	        }
	        else if(c == '3')
	        {
	            s.append(String.join("", Collections.nCopies(3, ("" + str.charAt(++i)))));
	        }
	        else if(c == '4')
	        {
	            s.append(String.join("", Collections.nCopies(4, ("" + str.charAt(++i)))));
	        }
	        else if(c == '5')
	        {
	            s.append(String.join("", Collections.nCopies(5, ("" + str.charAt(++i)))));
	        }
	        else if(c == '6')
	        {
	            s.append(String.join("", Collections.nCopies(6, ("" + str.charAt(++i)))));
	        }
	        else if(c == '7')
	        {
	            s.append(String.join("", Collections.nCopies(7, ("" + str.charAt(++i)))));
	        }
	    }
	    // Returns a string
	    return s.toString();
	}
	
	@Override
	public String convertToAscii(String binary)
	{
		String s2 = ""; 
		String[] temp = binary.split(" ");
		
		char nextChar;

		for(int i = 0; i < temp.length; i ++)
		{
		     nextChar = (char)Integer.parseInt(temp[i], 2);
		     s2 += nextChar;
		}
		return s2;
	}

//	public String getScore()
//	{
//    	score = new TwoBitScore(this.rawText.length(), this.decodedText.length());
//    	System.out.println(score.getScore());
//    	return ""+score.getScore();
//	}
	
    /**
     * This returns the decoded text after everything finishes successfully.
     * 
     * 
     * @see         {@link java.awt.Graphics}
     * @return		No return value.
     * @since       1.2
     */
	
	@Override
	public String getDecodedText()
	{
		return decodedText;
	}

	@Override
	public void nullifyVariables()
	{
		decodedText = "";
		unmapped = "";
		decompressed = "";
		
	}
}
