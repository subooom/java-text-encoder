package logic.interfaces;

import gui.DataPanel;

  /**
	* This interface sets the rules for those that implement it. It consists of the algorithm structure along with the
	* simple getters and setters methods written here and there.
	*
    * @author      Subham Kharel
    * @version     1.3
    * @since       1.2
    */

public interface Decryption
{
   /**
	* This method sets the stone form any decoding action. It is ruled as vital because without it there would be 
	* lots of NullPointerExceptions thrown.
	* 
	* @param String Text content
	* 
    * @author      Subham Kharel
    * @version     1.3
    * @since       1.2
	*/
	
	public void setText(String text);
	
	/**
     * This is practically the main method which does all the
     * good work. This method is called when
     * {@link java.awt.event.MouseListener}:clickevent is fired on the {@code decode} JButton.
     * 
     * @param		dp An instance of {@link gui.DataPanel}
     * 
     * @see         {@linkplain logic.Decoder}, {@link gui.ButtonGroup}
     * @return		No return value.
     * @since       1.2
     */
	
	public String decode(DataPanel dp);
	
	/**
     * {@code unMapFromUniqueCharacters} simply unmaps the `rawText` based on a static final variable
     * {@link logic.Encoder.ENCRYPTION_FORMAT} which is the last step done while encoding in this algorithm.
     * 
     * I've just added a bunch of if-statements to check for the matching character which I'm pretty sure could be
     * re-written in a much more beautiful manner while also taking care of the redundencies in code, but this is
     * what I'm going with for now.
     * 
     * @param		c A character to unmap
     * 
     * @see         {@linkplain logic.Encoder}
     * @return		No return value.
     * @since       1.2
     */
	
	public void unMapFromUniqueCharacters(char c);
	
	/**
     * {@code decompressString} uses {@linkplain java.lang.StringBuilder} and it's quite handy methods to decompress
     * the string. I make pretty good use of the  {@link java.util.Collections} class as well, using it's ncopies()
     * method. *grins*<br />
     * 
     * I've just added a bunch of if-statements to check for the matching character which I'm pretty sure could be
     * re-written in a much more beautiful manner while also taking care of the redundencies in code, but this is
     * what I'm going with for now.
     * 
     * @param		str A character to unmap
     * 
     * @see         {@linkplain logic.Encoder}
     * @return		No return value.
     * @since       1.2
     */
	
	public String decompressString(String str);
	
	
	
	public String convertToAscii(String binary);

    /**
     * This returns the decoded text after everything finishes successfully.
     * 
     * 
     * @see         {@link java.awt.Graphics}
     * @return		No return value.
     * @since       1.2
     */
	public String getDecodedText();
	
	public void nullifyVariables();
}
