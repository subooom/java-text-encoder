package logic.interfaces;

import gui.DataPanel;

   /**
	* This interface sets the rules for those that implement it. It consists of the open method.
	*
    * @author      Subham Kharel
    * @version     1.3
    * @since       1.2
    */

public interface FileImporting
{
   /**
	* This method sets the stone form any decoding action. It is ruled as vital because without it there would be 
	* lots of NullPointerExceptions thrown.
	* 
	* @param dataPanel An instance of {@link gui.DataPanel}
	* 
    * @author      Subham Kharel
    * @version     1.3
    * @since       1.2
	*/
	
	public void open(DataPanel dataPanel);
}
