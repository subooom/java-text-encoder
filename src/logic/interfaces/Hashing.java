package logic.interfaces;

/**
* This interface sets the rules for those that implement it. It consists of the algorithm structure along with the
* simple getters methods written here and there.
*
* @author      Subham Kharel
* @version     1.3
* @since       1.2
*/

public interface Hashing
{
	/**
	* This method generates a checksum value calculated.
	* 
	* @param Byte[] input Byte array
	* 
    * @author      Subham Kharel
    * @version     1.3
    * @since       1.2
	*/
	public String checksum(byte[] input);
	
	/**
     * This returns the hashed text after everything finishes successfully.
     * 
     * 
     * @see         {@link java.awt.Graphics}
     * @return		String hashed value
     * @since       1.2
     */
	
	public String getHashedValue();
}
