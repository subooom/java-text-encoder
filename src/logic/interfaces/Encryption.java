package logic.interfaces;

import gui.DataPanel;

  /**
	* This interface sets the rules for those that implement it. It consists of the algorithm structure along with the
	* simple getters and setters methods written here and there.
	*
    * @author      Subham Kharel
    * @version     1.3
    * @since       1.2
    */
public interface Encryption
{   
	/**
     * This method prepares the text for future puproses essentially by saving the text in an attribute.
     * It also initializes the byteArray.
     * 
     * @param 		text The text content supplied for decoding
     * 
     * @return		No return value.
     * @since       1.2
     */
	
	public void setText(String text);
	
	/**
     * This is the root and soul of {@linkplain logic.Encoder} which does all the
     * good work. This method is called when {@link java.awt.event.MouseListener}:clickevent
     * is fired on the {@code encode} JButton.
     * 
     * @param		dp An instance of {@link gui.DataPanel}
     * 
     * @see         {@linkplain logic.Decoder}, {@link gui.ButtonGroup}
     * @return		The encoded text.
     * @since       1.2
     */
	
	public String encode(DataPanel dp);
	
	/**
     * {@code mapToUniqueCharacters} simply maps the `compressed.charAt(j)` based on a static final variable
     * {@link logic.Encoder.ENCRYPTION_FORMAT} which is the first step done while decoding in this algorithm.
     * 
     * 
     * @param		c 		A character to unmap
     * @param		index	index of the current iteration
     * 
     * @see         {@linkplain logic.Decoder}
     * @return		mapped		The mapped value
     * @since       1.2
     */
	
	public String mapToUniqueCharacters(char c, int index);
	
	/**
     * {@code generateByteAt} simply generates the `rawText` 's equivalent of byte and stores it in an array.
     * {@link logic.Encoder.ENCRYPTION_FORMAT} which is the first step done while decoding in this algorithm.
     * 
     * 
     * @param		str				A character to unmap
     * @param		i	 			index of the current iteration
     * 
     * @see         {@linkplain logic.Decoder}
     * 
     * @return		byteArray[i]		The byte array
     * @since       1.2
     */
	
	public String generateByteAt(String str, int i);
	
	/**
     * {@code compressString} uses {@linkplain java.util.regex.Pattern} and its quite handy methods to compress
     * the string. I make pretty good use of the  {@link java.util.regex.Matcher} class as well, using it's group()
     * method. *grins*<br />
     * 
     * 
     * @param		str A string to compress
     * 
     * @see         {@linkplain logic.Encoder}
     * @return		compressed	Compressed output
     * @since       1.2
     */
	
	public String compressString(String str);
	
	/**
     * This returns the encoded text after everything finishes successfully.
     * 
     * 
     * @see         {@link java.awt.Graphics}
     * @return		encodedText The encoded output.
     * @since       1.2
     */
	
	
	public String getEncodedText();
	
	public void nullifyVariables();
}
