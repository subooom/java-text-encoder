package test;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Color;

import org.junit.jupiter.api.Test;

import gui.DataPanel;
import logic.Decoder;
import logic.Encoder;

/**
 * {@link test.EncodeTester} is the JUnit tester which
 * tests the accuracy of out encoding algorithm
 * 
 * @author      Subham Kharel
 * @version     1.4
 * @since       1.4
 * @see         {@link org.junit.jupiter.api.Assertions}
 */

class EncodeTester
{
	 /**
     * This is the method which is called when the testing is done.
     * It basically creates a new {@linkplain gui.DataBoard} instance, sets the text and encodes it
     * and tallys the output with the expected result.
     * 
     * 
     * @author      Subham Kharel
     * @since       1.4
     * @see         {@link javax.swing.UIManager}
     */
	
	@Test
	void test()
	{
		Encoder encoder  = new Encoder();
		Decoder decoder  = new Decoder();
		
		DataPanel dp = new DataPanel(Color.white, Color.blue, encoder, decoder);

		dp.getDataBoard().text("Hello world");
		String output = encoder.encode(dp);
		
		String expectedOutput = "!$A!@As$!$A!A!s$!A$!$As$!A$!$As$!A^!s!pAs@!A@!s$!A^!s@!$A!As$!A$!$As$!$A!$As";
		
		assertEquals(expectedOutput, output);
	}

}
