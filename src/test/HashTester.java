package test;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JTextArea;

import org.junit.jupiter.api.Test;

import gui.DataBoard;
import logic.Hasher;

/**
 * {@link test.HashTester} is the JUnit tester which
 * tests the accuracy of out hashing algorithm
 * 
 * @author      Subham Kharel
 * @version     1.4
 * @since       1.4
 * @see         {@link org.junit.jupiter.api.Assertions}
 */

class HashTester
{
	 /**
     * This is the method which is called when the testing is done.
     * It basically creates a new {@linkplain gui.DataBoard} instance, sets the text and encodes it
     * and tallys the output with the expected result.
     * 
     * 
     * @author      Subham Kharel
     * @since       1.4
     * @see         {@link javax.swing.UIManager}
     */
	
	@Test
	void test()
	{

		DataBoard db = new DataBoard(new JTextArea(), Color.white, Color.blue, new Font("IMPACT", Font.BOLD, 40));
	
		db.text("Hello world");
		
		Hasher hash  = new Hasher(db);
		
		int output = Integer.parseInt(hash.getHashedValue());
		
		int expectedOutput = 1359;
		
		assertEquals(expectedOutput, output);
	}

}
